cd D:\intozi_project\onpremise_anpr_dashboard
python manage.py makemigrations
python manage.py migrate
python manage.py loaddata onpremise_anpr_api/fixtures/register_vehicle_type-data.json
python manage.py loaddata onpremise_anpr_api/fixtures/register_vehicle_sync_type-data.json
python manage.py loaddata onpremise_anpr_api/fixtures/edge_device_type.json
python manage.py loaddata onpremise_anpr_api/fixtures/sync_status-data.json
python manage.py runserver